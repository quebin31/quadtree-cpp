#ifndef QTREE_QUAD_TREE_H
#define QTREE_QUAD_TREE_H 

#include "point.h"

namespace qtree {

struct Node {
	Point m_pos;
	int		m_data;

	Node()                                : m_pos(), m_data(0) {}
	Node(const Point& p, const int& data) : m_pos(p), m_data(data) {}
	Node(Point&& p, const int& data)      : m_pos(p), m_data(data) {}
	Node(const Point& p, int&& data)      : m_pos(p), m_data(data) {}
	Node(Point&& p, int&& data)           : m_pos(p), m_data(data) {}
};


class QTree {
private:
	Point m_top_left;	
	Point m_bot_right;

};

}

#endif
